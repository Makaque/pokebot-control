name := "Pokemon"

version := "1.0"

scalaVersion := "2.11.7"

//libraryDependencies += "com.github.jodersky" % "flow_2.11" % "2.3.0"

libraryDependencies += "com.typesafe" % "config" % "1.3.0"

run in Compile <<= Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run))

lazy val Pokemon = project.in(file(".")).settings(
  offline := true,
  libraryDependencies ++= Seq()
)

//lazy val processing = project.in(file("modules/processing"))