import processing.core.PApplet
import processing.serial.Serial
import com.typesafe.config.ConfigFactory

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer


class Pokebot extends PApplet {
  var ds: Serial = null

  var aBtn: Button = null
  var bBtn: Button = null
  var xBtn: Button = null
  var yBtn: Button = null
  var upBtn: Button = null
  var downBtn: Button = null
  var leftBtn: Button = null
  var rightBtn: Button = null

  val msgStart: Short = 255

  val buttons: mutable.ArrayBuffer[Button] = mutable.ArrayBuffer()

  var specialMode: Int = 0

  var controllerState: Int = 0
  var pControllerState: Int = 0

  override def setup() = {
    ds = new Serial(this, "/dev/ttyACM0", 9600)

    val buttonConf = Config.getConfig.getConfig("buttons")
    val keymap = buttonConf.getConfig("keymap")
    val buttonVals = buttonConf.getConfig("values")

    def initBtn(btn: String) = Button(keymap.getString(btn).toCharArray()(0), buttonVals.getInt(btn).toShort)(millis)

    aBtn = initBtn("a")
    bBtn = initBtn("b")
    xBtn = initBtn("x")
    yBtn = initBtn("y")
    upBtn = initBtn("up")
    downBtn = initBtn("down")
    leftBtn = initBtn("left")
    rightBtn = initBtn("right")

    buttons.append(aBtn, bBtn, xBtn, yBtn, upBtn, downBtn, leftBtn, rightBtn)
  }

  override def settings() = {
    size(200, 200)
  }

  override def draw() {
    readControllerState()
    if (controllerState != pControllerState) {
      outputControllerState()
    }
    pControllerState = controllerState
  }

  def outputControllerState() {
    println(msgStart)
    println(controllerState)
    ds.write(msgStart)
    ds.write(specialMode)
    ds.write(controllerState)
  }

  def readControllerState() {
    resetController()
    buttons.foreach(btn => {
      if (btn.pressed){
        controllerState |= btn.out
      }
    })
  }

  def resetController() = {
    controllerState = 0
  }

  override def keyPressed() = {
    for (i <- buttons.indices) {
      if (key == buttons(i).in) {
        buttons.update(i, buttons(i).press)
      }
    }
  }

  override def keyReleased() {
    for (i <- buttons.indices) {
      if (key == buttons(i).in) {
        buttons.update(i, buttons(i).release)
      }
    }
  }


}
