import cc.arduino.Arduino

/**
  * Created by mark on 29/02/16.
  */
class Timer(private val millis: () => Int, private val delay: Int){
  private val start: Long = millis() - delay
  def expired = millis() - start > delay
  def update = Timer(millis, 0)
}

object Timer{
  def apply(millis: () => Int, delay: Int = 100) = new Timer(millis, delay)
}
