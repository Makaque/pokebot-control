/**
  * Created by mark on 29/02/16.
  */
class Button(val in: Char, val out: Short, val released: Boolean, timer: Timer) {

  def pressed = !timer.expired || !released

  def press = Button(in, out, released = false, timer.update)

  def release = Button(in, out, released = true, timer)
}

object Button {
  def apply(in: Char, out: Short, released: Boolean = true)(millis: () => Int) =
    new Button(in, out, released, Timer(millis))

  def apply(in: Char, out: Short, released: Boolean, timer: Timer) =
    new Button(in, out, released, timer)
}
