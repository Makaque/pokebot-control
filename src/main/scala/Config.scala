import java.io.File

import com.typesafe.config.{Config => TSConfig, ConfigFactory}
import scala.collection.JavaConverters._


/**
  * Created by mark.geiger on 05/03/2016.
  */
object Config {

  def getConfig = {
    ConfigFactory.parseFile(new File("./config/application.conf")).resolve()
  }

  def valueList(conf: TSConfig) =     for {
    header <- conf.entrySet().asScala.toList
    key = header.getKey
    value = header.getValue.unwrapped().toString
  } yield (key, value)

}